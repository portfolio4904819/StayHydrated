# StayHydrated

1% decrease in total body water content can negatively affect our cognitive ability and mood. As the name of the project says Stay Hyderated . The objective of this automated script is to keep the users hyderated. 

## origin
I am a software developer by profession. There are times when I become absorbed in my work and don't realize how much time I am staring at the screen of my laptop. I would forget that I need to drink water and keep myself hydrated. Not drinking enough water has caused various problems for my health. My body is always dehydrated, leading to issues such as headaches, constipation, fatigue, and reduced urination.

I realized how important it is to drink water regularly. I felt that drinking less water would impact my kidneys, and improper digestion might lead to bowel issues. Fatigue would also affect both my work and personal life.

To overcome this issue, I tried various methods. I attempted to set alarms on my mobile phone, which worked initially. However, over time, I would simply silence the alarm and return to work without drinking water. Then, my 8-year-old niece suggested that I should get a smartwatch with reminders for water intake, among other things. I'm not someone who usually wears a watch, and adding another gadget to my life didn't feel comfortable. So, I sat down and thought about what I should do. Since I spend the whole day sitting and staring at my screen, I came up with a plan. I decided to create a Python script that would generate a pop-up in large letters, reminding me to drink water.

## How to install and run the Project

You will require python3.x . Refer to the requirements.txt to know about the libraries used. 

To install the dependencies use the below command
```python
pip install -r  requirements.txt
```


To test this project simply use the command 

```python
python3 main.py
```
When this script is ran , it produces a pop-up window which has text "Drink Water now!!"

<!-- <img title="a title" alt="Alt text" src="./src/Reminder.png"> -->

![Screenshot](src/assets/Reminder.png)


## Schedule the Scirpt

- Linux/mac users  can use [cron job](https://ostechnix.com/a-beginners-guide-to-cron-jobs/) 

- Windows users can use [Task Scheduler](https://www.jcchouinard.com/python-automation-using-task-scheduler/)

Based on your requirement set the scheduler to repeat this script.




