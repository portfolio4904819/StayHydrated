from PySide6.QtWidgets import (
    QApplication
    ,QWidget
    ,QLabel
    ,QVBoxLayout    
)

from PySide6.QtGui import QIcon,QFont
from PySide6.QtCore import QTimer

app         = QApplication([])
app.setStyle('macos')
window      = QWidget()
v_layout    = QVBoxLayout()

# btn1 = QPushButton('button 1')
label =QLabel("Drink Water now !! ")
label.move(100,100)
label.setFont(QFont("Times New Roman",100))
v_layout.addWidget(label)


window.setLayout(v_layout)
window.setGeometry(100,100,600,500)
window.show()
QTimer.singleShot(50000, window.close)
app.exec()
